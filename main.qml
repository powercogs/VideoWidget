import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0

ApplicationWindow
{
	id: mainWindow
	visible: true
	minimumWidth: 480
	minimumHeight: 270
	width: 480
	height: 270
	color: "transparent"
	flags: Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.BypassWindowManagerHint | Qt.X11BypassWindowManagerHint | Qt.MSWindowsOwnDC
	Component.onCompleted:
		menubar.state = "default"

	Material.theme: Material.Dark
	Material.accent: Material.BlueGrey

	property Rectangle global_menubar: menubar
	property real widget_opacity: 0.6
	property bool isforeground: true

	function loadVideo(text)
	{
		const ytlonglink = /^http[s]?:\/\/www\.youtube\.com\/watch\?v=([^&]+)(.*)/;
		const ytshortlink = /^http[s]?:\/\/youtu\.be\/([^&]+)(.*)/;
		const ytembedlinkbase = "https://www.youtube.com/embed/";

		var ytid;
		print("Entered text is: " + text);
		if(ytlonglink.test(text))
		{
			print("Input matches Youtube link form 1");
			ytid = text.match(ytlonglink)[1];
		}
		else if(ytshortlink.test(text))
		{
			print("Input matches Youtube link form 2");
			ytid = text.match(ytshortlink)[1];
		}
		else
		{
			print("Input matches none of the defined link patterns.");
			urlinputbar.state = "error";
			return;
		}
		print("Video id is " + ytid);
		var browserlink = ytembedlinkbase + ytid;
		print("Embed link is " + browserlink);
		print("Loading Url...");
		urlinputbar.state = "hide";
		blankpage.visible = false;
		browser.url = browserlink;
	}

	Action
	{
		shortcut: "Ctrl+V"
		onTriggered: loadVideo(helperfunctions.clipboardtext)
	}

	Rectangle
	{
		id: background
		anchors.fill: parent
		color: "transparent"
		radius: 2
		antialiasing: true
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.verticalCenter: parent.verticalCenter



		MouseArea
		{
			id: browsermousearea
			enabled: true
			anchors.fill: parent
			hoverEnabled: true
			propagateComposedEvents: true
			preventStealing: true

			onContainsMouseChanged:
			{
				if (containsMouse)
					menubar.state = "show"
				else
					menubar.state = "hide"
			}

			WebView
			{
				id: browser
				anchors.fill: parent
				//url:"http://player.twitch.tv/?channel=asusrog"
				//url: "https://www.youtube.com/embed/RC7Jg7sVTms"
				Component.onCompleted:
					runJavaScript("document.write('<!DOCTYPE html><html><body bgcolor=\"#424242\"></body></html>')")
				onLoadingChanged:
					if(!loading)
						menubar.state = browsermousearea.containsMouse ? "show" : "hide"
			}

			Item
			{
				id: blankpage
				anchors.fill: parent
				opacity: browser.opacity

				Image
				{
					anchors.verticalCenter: parent.verticalCenter
					anchors.horizontalCenter: parent.horizontalCenter
					sourceSize: Qt.size(300 , 300)
					antialiasing: true
					source: "qrc:/img/blanktext.svg"
				}
			}
		}

		MouseArea
		{
			id: resizehandle
			anchors.right: parent.right
			anchors.bottom: parent.bottom
			width: 5
			height: 5
			cursorShape: Qt.SizeFDiagCursor
			property variant clickPos:  "1,1"

			onPressed:
			{
				clickPos  = Qt.point(mouse.x,mouse.y)
			}

			onPositionChanged:
			{
				var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
				mainWindow.width += delta.x;
				mainWindow.height += delta.y;
			}
		}

		Menubar { id: menubar }

		UrlInputBar
		{
			id: urlinputbar
			anchors.top: menubar.bottom
			onUrlEntered: loadVideo(text)
		}

		OpacityBar
		{
			id: opacitybar
			anchors.right: parent.right
			anchors.top: menubar.bottom
			onSliderMoving: {} //browser.opacity = newvalue
			onSliderMoved:
			{
				widget_opacity = newvalue
			}
		}

	}

	TrayIcon { id: tray }
}

import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0


Rectangle
{
    id: urlinputbar
    width: parent.width
    height: 40
    color: "#e0000000"
    Component.onCompleted: state = "hide"
    
	signal urlEntered(variant text)
    
    states:
	[
		State
		{
			name: "hide"
			PropertyChanges { target: urlinputbar; height: 0 }
			PropertyChanges { target: inputline; opacity: 0 }
			PropertyChanges { target: urlinput; opacity: 0 }
			PropertyChanges { target: urlinput; visible: true }
			PropertyChanges { target: urlinput; focus: false }
			PropertyChanges { target: urlgobutton; focus: false }
			PropertyChanges { target: urlgobutton; opacity: 0 }
			PropertyChanges { target: urlgobutton; visible: false }
		},
		State
		{
			name: "show"
			PropertyChanges { target: urlinputbar; height: 40 }
			PropertyChanges { target: inputline; opacity: 1 }
			PropertyChanges { target: urlinput; opacity: 1 }
			PropertyChanges { target: urlinput; visible: true }
			PropertyChanges { target: urlinput; focus: true }
			PropertyChanges { target: urlerrortext; opacity: 0 }
			PropertyChanges { target: urlerrortext; visible: false }
			PropertyChanges { target: urlgobutton; opacity: 1 }
			PropertyChanges { target: urlgobutton; visible: true }
		},
		State
		{
			name: "error"
			PropertyChanges { target: urlinputbar; height: 56 }
			PropertyChanges { target: urlerrortext; opacity: 1 }
			PropertyChanges { target: urlerrortext; visible: true }
		}
    ]
    transitions:
	[
		Transition
		{
			from: "hide"
			to: "show"
			SequentialAnimation
			{
				PropertyAnimation { target: urlinput; property: "focus"; duration: 1 }
				PropertyAnimation { target: urlinputbar; property: "height"; duration: 200; easing.type: Easing.OutQuad }
				PropertyAnimation { target: urlinput; property: "visible"; duration: 1 }
				PropertyAnimation { target: urlgobutton; property: "visible"; duration: 1 }
				ParallelAnimation
				{
					PropertyAnimation { target: inputline; property: "opacity"; duration: 50 }
					PropertyAnimation { target: urlinput; property: "opacity"; duration: 50 }
					PropertyAnimation { target: urlgobutton; property: "opacity"; duration: 50 }
				}
			}
		},
		Transition
		{
			from: "show"
			to: "hide"
			SequentialAnimation
			{
				PropertyAnimation { target: urlinput; property: "focus"; duration: 1 }
				ParallelAnimation
				{
					PropertyAnimation { target: inputline; property: "opacity"; duration: 50 }
					PropertyAnimation { target: urlinput; property: "opacity"; duration: 50 }
					PropertyAnimation { target: urlgobutton; property: "opacity"; duration: 50 }
				}
				PropertyAnimation { target: urlinput; property: "visible"; duration: 1 }
				PropertyAnimation { target: urlgobutton; property: "visible"; duration: 1 }
				PropertyAnimation { target: urlinputbar; property: "height"; duration: 50; easing.type: Easing.InQuad }
			}
		},
		Transition
		{
			to: "error"
			reversible: true
			SequentialAnimation
			{
				PropertyAnimation { target: urlinputbar; property: "height"; duration: 100; easing.type: Easing.InQuad }
				PropertyAnimation { target: urlerrortext; property: "visible"; duration: 1 }
				PropertyAnimation { target: urlerrortext; property: "opacity"; duration: 50 }
			}
		}

    ]
    
    Rectangle
    {
		id: inputline
		anchors
		{
			top: parent.top
			topMargin: 30
			horizontalCenter: parent.horizontalCenter
		}
		height: 1
		width: parent.width-10
		color: "#e0e0e0"
    }

	Item
	{
		id: urlgobutton
		anchors
		{
			bottom: inputline.top
			right: parent.right
			leftMargin: 2
		}
		width: 30
		height: width

		Image
		{
			anchors.verticalCenter: parent.verticalCenter
			anchors.horizontalCenter: parent.horizontalCenter
			sourceSize: Qt.size(25, 25)
			source: "qrc:/img/ic_arrow_forward_white_48px.svg"
		}

		MouseArea
		{
			anchors.fill: parent
			onClicked:
			{
				urlEntered(urlinput.text)
				urlinput.clear()
			}
		}
	}
    
    TextInput
    {
		id: urlinput
		anchors
		{
			bottom: inputline.top
			left: inputline.left
			right: urlgobutton.left
			bottomMargin: 8
		}
		color: inputline.color
		font.pointSize: 10
		inputMethodHints: Qt.ImhUrlCharactersOnly
		onAccepted:
		{
			urlEntered(text);
			clear();
		}
		Keys.onEscapePressed:
		{
			clear()
			parent.state = "hide"
		}
    }

	Text
	{
		id: urlerrortext
		anchors
		{
			top: inputline.bottom
			horizontalCenter: inputline.horizontalCenter
		}
		width: inputline.width
		height: 16
		text: qsTr("Url not recognized")
		textFormat: Text.PlainText
		verticalAlignment: Text.AlignBottom
		font.pointSize: 8
		color: "#d50000"
		opacity: 0
		visible: false
	}
}

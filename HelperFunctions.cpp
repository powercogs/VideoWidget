#include <HelperFunctions.h>
#include <QGuiApplication>
#include <QClipboard>
#include <QFile>
#include <QSettings>

HelperFunctions::HelperFunctions(QObject *parent) : QObject(parent) { }

QString HelperFunctions::clipboardtext() const
{
	return QGuiApplication::clipboard()->text();
}

QString HelperFunctions::loadResourceAsText(QString filepath) const
{
	QFile loadfile(filepath);
	loadfile.open(QIODevice::ReadOnly | QIODevice::Text);
	QString textdata = loadfile.readAll();
	loadfile.close();
	return textdata;
}

void HelperFunctions::saveSettingToINI(QString key, QVariant value)
{
	QSettings settings("settings.ini", QSettings::IniFormat);
	settings.setValue(key, value);
}

QVariant HelperFunctions::loadSettingFromINI(QString key, QVariant ifnotexist) const
{
	QSettings settings("settings.ini", QSettings::IniFormat);
	if(settings.contains(key))
		return settings.value(key);
	else
		return ifnotexist;
}

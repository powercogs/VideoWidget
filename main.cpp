#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtWebView>
#include <QResource>
#include <QQuickStyle>

#include <HelperFunctions.h>



int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

	// Prevent resize flickering
	QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

    QGuiApplication app(argc, argv);
    QtWebView::initialize();
	QQuickStyle::setStyle("Material");

	QCoreApplication::setOrganizationName("kek");
	QCoreApplication::setApplicationName("VideoWidget");

    QQmlApplicationEngine engine;

	engine.rootContext()->setContextProperty("helperfunctions", new HelperFunctions());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

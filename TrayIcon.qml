import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0


SystemTrayIcon
{
    id: tray
    visible: true
	iconSource: "qrc:/img/tray.svg"
    tooltip: "VideoWidget"
    menu:
		Menu
		{
			MenuItem
			{
				text: qsTr("Always on top")
				checkable: true
				checked: isforeground
				onCheckedChanged:
				{
					if (checked)
					{
						mainWindow.flags |= Qt.WindowStaysOnTopHint
						isforeground = true
					}
					else
					{
						mainWindow.flags &= ~Qt.WindowStaysOnTopHint
						isforeground = false
					}
				}
			}
			MenuItem
			{
				text: qsTr("Quit")
				onTriggered: Qt.quit()
			}
		}
}

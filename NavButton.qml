import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0


Item
{
	id: navButton
	anchors
	{
		verticalCenter: parent.verticalCenter
		margins: 2
	}
	height: parent.height-4
	width: height

	signal navClicked()
	property color hover_color
	property url iconsrc
	property string tooltipText: "ToolTip"

	Rectangle
	{
		id: btbg
		anchors.centerIn: parent
		width: parent.width
		height: parent.height
		color: hover_color
		radius: width*0.5
		Component.onCompleted: btbg.state = "default"

		states:
		[
			State
			{
				name: "default"
				PropertyChanges { target: btbg; width: parent.width* 0.5 }
				PropertyChanges { target: btbg; height: parent.height* 0.5 }
				PropertyChanges { target: btbg; opacity: 0 }
			},
			State
			{
				name: "hover"
				PropertyChanges { target: btbg; width: parent.width* 0.8 }
				PropertyChanges { target: btbg; height: parent.height * 0.8 }
				PropertyChanges { target: btbg; opacity: 1 }
			}
		]
		transitions:
		[
			Transition
			{
				to: "hover"
				reversible: true
				ParallelAnimation
				{
					PropertyAnimation { property: "width"; duration: 100; easing.type: Easing.OutQuad }
					PropertyAnimation { property: "height"; duration: 100; easing.type: Easing.OutQuad }
					PropertyAnimation { property: "opacity"; duration: 400; }
				}
			}

		]
	}

	Image
	{
		anchors.verticalCenter: parent.verticalCenter
		anchors.horizontalCenter: parent.horizontalCenter
		sourceSize: Qt.size(20, 20)
		source: iconsrc
	}

	MouseArea
	{
		anchors.fill: parent
		hoverEnabled: true
		onContainsMouseChanged:
		{
			if(containsMouse)
			{
				btbg.state = "hover"
				global_menubar.state = "show"
			}
			else
			{
				btbg.state = "default"
			}
		}
		onClicked: navClicked()

		ToolTip.delay: 750
		ToolTip.visible: containsMouse
		ToolTip.text: tooltipText
	}
}

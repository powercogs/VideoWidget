#pragma once

#include <QObject>
#include <QVariant>

class HelperFunctions : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString clipboardtext READ clipboardtext)
public:
	explicit HelperFunctions(QObject* parent = nullptr);
	QString clipboardtext() const;
	Q_INVOKABLE QString loadResourceAsText(QString filepath) const;
	Q_INVOKABLE void saveSettingToINI(QString key, QVariant value);
	Q_INVOKABLE QVariant loadSettingFromINI(QString key, QVariant ifnotexist) const;
};

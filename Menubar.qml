import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0


Rectangle
{
	id: menubar
	anchors.top: parent.top
	height:	30
	width: parent.width
	color: "transparent"

	DropShadow
	{
		id: menubarshadow
		anchors.fill: parent
		verticalOffset: 2
		radius: 8.0
		samples: 17
		color: "#80000000"
		source: parent
	}

	states:
	[
		State
		{
			name:"default"
			PropertyChanges { target: background; color: "#000" }
		},
		State
		{
			name: "show"
			PropertyChanges { target: menubar; anchors.topMargin: 0 }
			PropertyChanges { target: browser; opacity: 1 }  // ff
			PropertyChanges { target: menubarshadow; visible: true }
		},
		State
		{
			name: "hide"
			PropertyChanges { target: menubar; anchors.topMargin: -(menubar.height) }
			PropertyChanges { target: browser; opacity: widget_opacity }
			PropertyChanges { target: menubarshadow; visible: false }
		},
		State
		{
			name:"preview"
			PropertyChanges { target: menubar; anchors.topMargin: 0 }
			PropertyChanges { target: browser; opacity: widget_opacity }
			PropertyChanges { target: menubarshadow; visible: true }
		}
	]
	transitions:
	[
		Transition
		{
			from: "show"
			to: "hide"
			SequentialAnimation
			{
				PropertyAnimation { target: menubar; property: "anchors.topMargin"; duration: 500; easing.type: Easing.InQuart}
				PropertyAnimation { target: menubarshadow; property: "visible"; duration: 10 }
				PropertyAnimation { target: browser; property: "opacity"; duration: 400; easing.type: Easing.OutQuad }
			}

		},
		Transition
		{
			from: "hide"
			to: "show"
			SequentialAnimation
			{
				PropertyAnimation { target: browser; property: "opacity"; duration: 10}
				PropertyAnimation { target: menubarshadow; property: "visible"; duration: 10 }
				PropertyAnimation { target: menubar; property: "anchors.topMargin"; duration: 50 }
			}
		}
	]

	FastBlur
	{
		anchors.fill: parent
		radius: 32
		opacity: 0.9
		source:
			ShaderEffectSource
			{
				anchors.fill: parent
				sourceItem: browser
				sourceRect: Qt.rect(menubar.x, menubar.y, menubar.width, menubar.height)
				recursive: true
			}
	}

	// for increased contrast
	Rectangle
	{
		anchors.fill: parent
		color: "#a0000000"
	}

	MouseArea
	{
		id: menubarmousearea
		anchors.fill: parent
		propagateComposedEvents: true
		property variant clickPos:  "1,1"
		onPressed:
		{
			clickPos  = Qt.point(mouse.x,mouse.y)
		}
		onPositionChanged:
		{
			var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
			mainWindow.x += delta.x;
			mainWindow.y += delta.y;
		}
	}

	NavButton
	{
		id: closebutton
		anchors.right: parent.right
		hover_color: "#c62828"
		onNavClicked: Qt.quit()
		iconsrc: "qrc:/img/ic_close_white_64px.svg"
		tooltipText: qsTr("Exit")
	}

	NavButton
	{
		id: addbutton
		anchors.left: parent.left
		hover_color: "#00c853"
		onNavClicked:
		{
			opacitybar.state = "hide"
			urlinputbar.state = (urlinputbar.state === "show" || urlinputbar.state === "error") ? "hide" : "show"
		}
		iconsrc: "qrc:/img/ic_add_white_48px.svg"
		tooltipText: qsTr("Load video")
	}

	NavButton
	{
		id: opacitybutton
		anchors.right: closebutton.left
		hover_color: "#607d8b"
		onNavClicked:
		{
			urlinputbar.state = "hide"
			opacitybar.state = (opacitybar.state === "show" || opacitybar.state === "error") ? "hide" : "show"
		}
		iconsrc: "qrc:/img/ic_opacity_white_48px.svg"
		tooltipText: qsTr("Opacity")
	}

	NavButton
	{
		id: fgtogglebutton
		anchors.right: opacitybutton.left
		hover_color: "#607d8b"
		onNavClicked:
		{
			if((mainWindow.flags & Qt.WindowStaysOnTopHint) > 0 )
			{
				mainWindow.flags &= ~Qt.WindowStaysOnTopHint
				isforeground = false
			}
			else
			{
				mainWindow.flags |= Qt.WindowStaysOnTopHint
				isforeground = true
			}
		}
		iconsrc: isforeground ? "qrc:/img/ic_flip_to_back_white_48px.svg" : "qrc:/img/ic_flip_to_front_white_48px.svg"
		tooltipText: qsTr("Toggle sticky")
	}

}

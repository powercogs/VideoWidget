import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0
import Qt.labs.platform 1.0
import QtWebView 1.1
import Qt.labs.calendar 1.0

Rectangle
{
    id: opacitybar
    width: 250
    height: 40
	color: "#e0000000"
	Component.onCompleted: state = "hide"
    
    signal sliderMoved(variant newvalue)
	signal sliderMoving(variant newvalue)
    
    states:
	[
		State
		{
			name: "hide"
			PropertyChanges { target: opacitybar; height: 0 }
			PropertyChanges { target: opacityslider; opacity: 0 }
			PropertyChanges { target: opacityslider; visible: false }
			PropertyChanges { target: opacityslider; focus: false }
			PropertyChanges { target: opacityicon; opacity: 0 }
			PropertyChanges { target: opacityicon; visible: false }
		},
		State
		{
			name: "show"
			PropertyChanges { target: opacitybar; height: 40 }
			PropertyChanges { target: opacityslider; opacity: 1 }
			PropertyChanges { target: opacityslider; visible: true }
			PropertyChanges { target: opacityslider; focus: true }
			PropertyChanges { target: opacityicon; opacity: 1 }
			PropertyChanges { target: opacityicon; visible: true }
		}
    ]
    transitions:
	[
		Transition
		{
			from: "hide"
			to: "show"
			SequentialAnimation
			{
				PropertyAnimation { target: opacitybar; property: "height"; duration: 200; easing.type: Easing.OutQuad }
				PropertyAnimation { target: opacityicon; property: "visible"; duration: 1 }
				PropertyAnimation { target: opacityslider; property: "visible"; duration: 1 }
				ParallelAnimation
				{
					PropertyAnimation { target: opacityicon; property: "opacity"; duration: 50 }
					PropertyAnimation { target: opacitybar; property: "opacity"; duration: 50 }
				}
			PropertyAnimation { target: opacityslider; property: "focus"; duration: 1 }
			}
		},
		Transition
		{
			from: "show"
			to: "hide"
			SequentialAnimation
			{
				PropertyAnimation { target: opacityslider; property: "focus"; duration: 1 }
				ParallelAnimation
				{
					PropertyAnimation { target: opacityicon; property: "opacity"; duration: 50 }
					PropertyAnimation { target: opacitybar; property: "opacity"; duration: 50 }
				}
				PropertyAnimation { target: opacityslider; property: "visible"; duration: 1 }
				PropertyAnimation { target: opacitybar; property: "height"; duration: 50; easing.type: Easing.InQuad }
			}
		}
    ]
    
    Image
    {
		id: opacityicon
		antialiasing: true
		anchors
		{
			verticalCenter: parent.verticalCenter
			left: parent.left
			leftMargin: 10
			rightMargin: 10
		}
		sourceSize: Qt.size(20, 20)
		source: "qrc:/img/ic_opacity_white_48px.svg"
    }

	MouseArea
	{
		id: opacitymousearea
		anchors.fill: parent
		hoverEnabled: true
		onContainsMouseChanged:
		{
			if(containsMouse)
				global_menubar.state = "preview"
			else
				parent.state = "hide"
		}
	}
    
    Slider
    {
		id: opacityslider
		anchors
		{
			left: opacityicon.right
			right: parent.right
			rightMargin: 10
			verticalCenter: parent.verticalCenter
		}
		height: parent.height
		antialiasing: true
		hoverEnabled: false
		from: 0.2
		to: 1
		Component.onCompleted: value = widget_opacity * (to - from) + from
		onValueChanged: sliderMoved(value)
		onPressedChanged:
		{
			if(pressed)
			{
				global_menubar.state = "preview"
				sliderMoving(value)
			}
			else
				sliderMoved(value)
		}
		Keys.onEscapePressed: parent.state = "hide"
    }
}
